/* 
To compile :
$ g++ Count.cpp -o CountMe

To execute:
$ CountMe
*/

/* std classes */
#include <iostream>
#include <string>
#include <stdlib.h>
#include <cstdio> // for printf()

struct Card{
	std::string Value;
	bool Taken;
};

int main(int argc, char* argv[])
{
	bool test = false;
	if( argc >= 2 )
		test = true;

	int NbCards=52;

	std::string Cards[52]={	"2","3","4","5","6","7","8","9","10","V","D","R","A",
				"2","3","4","5","6","7","8","9","10","V","D","R","A",
				"2","3","4","5","6","7","8","9","10","V","D","R","A",
				"2","3","4","5","6","7","8","9","10","V","D","R","A"	};

	Card Deck[52];
	for(int i=0;i<NbCards;i++)
	{
		Deck[i].Value = Cards[i];
		Deck[i].Taken = false;
	}

	int TimeDeckS=0;
	while( TimeDeckS <=0 )
	{
		std::cout<<"| Please give the time to finish the deck, in seconds : ";
		if( test )
			TimeDeckS = 30;
		else
			std::cin>>TimeDeckS;
	}
	int StepMs = (int) ( TimeDeckS*1000/NbCards ) ; // ms
	std::cout<<"| The time step between 2 cards will be : "<< StepMs <<" ms."<<std::endl;
	std::cout<<"| Ready to count ? Press ENTER when ready !";
	if( ! test )
	{
		char Enter;
		std::cin.get(Enter); // for the enter of the first cin
		std::cin.get(Enter); // wait for press enter
	}
	std::cout<<"|"<<std::endl;

	srand ( time(NULL) ); // initialize random counter with the system clock ( changes every second )

	for(int i=0;i<NbCards;i++)
	{
		int Index = rand() % NbCards;
		while( Deck[Index].Taken ) Index = rand() % NbCards;

		if( Deck[Index].Value == "10" )std::cout<<"[ "<< Deck[Index].Value <<" ]\r"<<std::endl ;
		else std::cout<<"[  "<< Deck[Index].Value <<" ]\r"<<std::endl ;

		Deck[Index].Taken=true;
		usleep(StepMs*1000);
	}

	std::cout<<"| DONE         \n| If your count is 0, then you're good :)"<<std::endl;

	return 0;
}
